---
title: Home
description: DIV Compo
layout: layout.html
---

# ¡Hola, Mundo!

¿Pero esto qué es? La idea es hacer una competición con el espíritu de 1997 usando las versiones
originales de [DIV Games Studio][div-games-studio] pero teniendo en cuenta que ahora
tenemos una décima parte del tiempo libre que teníamos hace 20 años. Así que esto no va a ser
una Jam trepidante de 48 o 72 horas.

Y recordad...

![This was HD in 1997](/images/this-was-hd.png)

## Competición

La competición comienza el **1 de febrero a las 00:00:00** y ~~termina el 29 de febrero a las 23:59:59.~~ :warning: **PRORROGADO**!! : el **31 de marzo a las 23:59:59** :warning:

El género de los juegos será **matamarcianos** y la resolución máxima será **640x480**.

Lo suyo sería subir un repositorio a [GitHub][github] o al servicio que preferiais e ir subiendo ahí los
avances de nuestro juego. Si queréis usar [Dropbox][dropbox], [Google Drive][google-drive] o cualquier otro
servicio que no sea `git` me parece bien, la cuestión es que esté disponible en algún lugar público.

Los juegos deberían correr en un [DOSBox][dosbox] o en una máquina virtual con MS-DOS 6.22 sin necesidad
de tener que instalar o copiar nada más salvo el juego.

Se pueden usar herramientas modernas como [Blender][blender], Photoshop, Procreate, [GIMP][gimp] o cualquier
otra herramienta que sirva para crear los gráficos, sonidos o músicas. Siempre y cuando el juego
funcione en alguno de los dos DIVs originales y que pueda correr en MS-DOS.

---

# Instalar DIV

Puedes descargar en los siguientes enlaces una copia de [DIV Games Studio][div1-download] y
[DIV 2 Games Studio][div2-download].

Para hacerlo funcionar hay varias opciones:
- Instalarlo en un ordenador de la época
- Utilizar [DOSBox][dosbox]
- Utilizar una máquina virtual con MS-DOS 6.22 o Windows 95

## Instalar DOSBox

### Windows

Si utilizas [Chocolatey][chocolatey] puedes utilizar el siguiente comando:

```
choco install dosbox
```

O bien puedes descargarte el ejecutable directamente de la página web de [DOSBox][dosbox-download].

### Ubuntu

```
sudo apt install dosbox
```

### macOS

La manera más sencilla de instalar DOSBox en macOS es utilizar [Homebrew][homebrew] e instalarlo
usando:

```
brew cask install dosbox
```

## Ejecutar DOSBox

Una vez que tenemos [DOSBox][dosbox] instalado la manera más sencilla de ejecutar DIV es copiar
todos los archivos de DIV en una carpeta y montar esa carpeta con:

```
dosbox .
```

Si quieres puedes montarte un `dosbox.conf` en la raiz de la carpeta que quieres que sea tu
"disco duro" de DOS. El archivo `dosbox.conf` permite configurar un montón de cosas de DOSBox, como
la velocidad del ordenador, la RAM disponible o la manera en la que se renderiza.

```
[sdl]
# En macOS y Linux este es el mejor renderer
output=opengl

# Si quieres ponerlo en modo a pantalla completa
#fullscreen=true
#fullresolution=1280x720
# Si quieres ponerlo en modo ventana es recomendable también utilizar
# una resolución de ventana para poder trabajar más comodamente.
windowresolution=1280x800

[dos]
# Ponemos el layout del teclado en castellano.
keyboardlayout=sp

[joystick]
# Configuramos el Joystick, hay varios valores para probar:
# - none: Desactiva el joystick (esto sirve para evitar conflictos con
#         el Key Remapper (se abre con CTRL-F1) de DOSBox.
# - 2axis: 2 ejes y 2 botones.
# - 4axis: 4 ejes y 4 botones o gamepad con 2 ejes y 6 botones.
# - 4axis_2: ??? ni idea de qué hace esto.
# - fcs: ThrustMaster FlightStick.
# - ch: CH FlightStick.
joysticktype=auto

[ipx]
# Activamos IPX. He hecho varias pruebas en DIV con las funciones de red
# y no funciona ni en IPX, ni usando el serial. Si consigues que funcione,
# por favor, dime cómo.
ipx=true

[serial]
# Usando nullmodem podemos redirigir el tráfico de los puertos COM1 y COM2
# del ordenador a un servicio TCP que los enrute de otra manera.
serial1=nullmodem server:127.0.0.1 port:5000
serial2=nullmodem server:127.0.0.1 port:5000
# También podemos usar el modo modem o directserial
#serial3=modem
#serial4=directserial
```

## Instalar MS-DOS 6.22

Utilizando [VirtualBox][virtual-box] se puede crear una máquina virtual que
corra [MS-DOS][msdos-download]. Lo suyo sería crear una máquina virtual que
no exceda la capacidad (al menos en memoria) de los ordenadores de la época.

Un ordenador modelo podría ser:

- Procesador: 486 DX2 66 MHz
- Memoria: 16 MB RAM

[msdos-download]: https://archive.org/details/MSDOS6.22_201905

[virtual-box]: https://www.virtualbox.org/

[chocolatey]: https://chocolatey.org/
[dosbox]: https://www.dosbox.com/
[dosbox-download]: https://www.dosbox.com/download.php?main=1
[homebrew]: https://brew.sh/index_es

[div-games-studio]: https://es.wikipedia.org/wiki/DIV_Games_Studio

[div1-download]: https://drive.google.com/open?id=1zkSkK-F_H01oDxuE9Sw8zflwE17GdZAY
[div2-download]: https://drive.google.com/open?id=1Qc7IgcHU4cReJM0NSeCVKxUF1eUpA6mF

[github]: https://github.com
[dropbox]: https://dropbox.com
[google-drive]: https://drive.google.com

[blender]: https://www.blender.org/
[gimp]: https://www.gimp.org/
[inkscape]: https://inkscape.org/
