# DIV Compo

Esta página web ha sido creada utilizando [Metalsmith](https://metalsmith.io/) y [Now](now.sh).

## Colaborar

Si queréis hacer alguna modificación a la página web sólo tenéis que hacer un fork del repositorio
y hacer un Pull Request con tus cambios. Una vez que los revise y sean aceptados la página se
desplegará automáticamente en [divcompo.now.sh](divcompo.now.sh).

Made with :heart: by [AzazelN28](https://github.com/AzazelN28)
